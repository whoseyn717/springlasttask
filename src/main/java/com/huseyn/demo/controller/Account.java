package com.huseyn.demo.controller;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class Account {

     String name;
     String surname;
     String password;
     Integer id;
}
