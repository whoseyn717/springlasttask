package com.huseyn.demo.controller;

import com.huseyn.demo.service.AccountService;
import com.huseyn.demo.service.AccountServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{id}")
    public Account get(@PathVariable Long id) {
        return accountService.get(id);

        }

@PostMapping("/create")
    public Account create(@RequestBody Account account){
    return accountService.create(account);

}
@PutMapping("/update")
    public Account update(@RequestBody Account account){
    return accountService.update(account);
}
@DeleteMapping("/delete{id}")
    public void delete(@PathVariable Long id){
accountService.delete(id);
    }


}


