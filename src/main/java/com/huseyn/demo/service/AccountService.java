package com.huseyn.demo.service;

import com.huseyn.demo.controller.Account;

public interface AccountService {
    Account get(Long id);

    Account create(Account account);

    Account update(Account account);

    void delete(Long id);
}
