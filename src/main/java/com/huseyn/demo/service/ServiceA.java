package com.huseyn.demo.service;

import org.springframework.context.annotation.Lazy;

public class ServiceA {
    private final ServiceB serviceB;

    public ServiceA(@Lazy ServiceB serviceB) {
        this.serviceB = serviceB;
    }
}
